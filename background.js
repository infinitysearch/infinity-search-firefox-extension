function openPage() {
  browser.tabs.create({
    url: "https://infinitysearch.co"
  });
}

browser.browserAction.onClicked.addListener(openPage);
