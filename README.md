# Infinity Search Firefox Extension 

Open [Infinity Search](https://www.infinitysearch.co) in a new tab to make a quick search with this extension.

## Installation

- [Download Our Extension From the Firefox Store](https://addons.mozilla.org/en-US/firefox/addon/infinity-search/)



